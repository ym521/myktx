package com.ym521.mykotlinext

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import androidx.core.view.WindowInsetsAnimationCompat
import androidx.core.view.WindowInsetsCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.ym521.mykotlinext.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    lateinit var viewBind: ActivityMainBinding
    lateinit var msgAdapter: MessageAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewBind = ActivityMainBinding.inflate(layoutInflater)
        msgAdapter = MessageAdapter()
        setContentView(viewBind.root)
        initKeyboardAmin()
        viewBind.apply {
            rlvMassage.apply {
                adapter = msgAdapter
                itemAnimator = null
                layoutManager = LinearLayoutManager(this@MainActivity)
            }
            btnSend.setOnClickListener {
                val content = viewBind.etSend.text.toString().trim()
                if (content.isNotBlank())
                    msgAdapter.addMessage(content)
                viewBind.etSend.setText("")
                viewBind.rlvMassage.scrollToPosition(msgAdapter.count() - 1)
            }
        }
    }

    private fun initKeyboardAmin() {
        var startBottom = 0f
        var endBottom = 0f
        Log.e("[TAG]","initKeyboardAmin()")
        ViewCompat.setWindowInsetsAnimationCallback(viewBind.root,
            object : WindowInsetsAnimationCompat.Callback(DISPATCH_MODE_STOP) {

                override fun onPrepare(animation: WindowInsetsAnimationCompat) {
                    Log.e("[TAG]","onPrepare()")
                    startBottom = viewBind.root.bottom.toFloat()
                }

                override fun onStart(
                    animation: WindowInsetsAnimationCompat,
                    bounds: WindowInsetsAnimationCompat.BoundsCompat
                ): WindowInsetsAnimationCompat.BoundsCompat {
                    Log.e("[TAG]","onStart()")
                    endBottom = viewBind.root.bottom.toFloat()
                    return bounds
                }

                override fun onProgress(
                    insets: WindowInsetsCompat,
                    runningAnimations: MutableList<WindowInsetsAnimationCompat>
                ): WindowInsetsCompat {
                    Log.e("[TAG]","onProgress()")
                    val imeAnimation = runningAnimations.find {
                        it.typeMask and WindowInsetsCompat.Type.ime() != 0
                    } ?: return insets

                    val translationY =
                        (startBottom - endBottom) * (1 - imeAnimation.interpolatedFraction)
                    Log.e("[TAG]","${translationY}")
                    viewBind.root.translationY = translationY
                    return insets
                }

            })
    }
}