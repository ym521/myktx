# MyKtx

#### 介绍
Android 常用的自定义的扩展；
注意：大部分扩展都是基于Kotlin，部分自定义扩展是java；

#### 说明

1. layoutManager： 关于 recycleView 相关的自定义；
2. viewPager2：内有解决其滑动冲突的方案，NestedScrollableHost 处于两个Viewpage2 (XML布局中)中间即可；
3. ByteExt: 关于 Byte的转换扩展
4. DimensionExt：关于 dp、px、sp 等转换
5. Res： Android XML资源的获取的扩展，注意先在application中 初始化 调用init(application)
6. StringExt：关于String的转换扩展和加密扩展
7. SoftInputKeyBoardHelper：软键盘的显示和隐藏
8. WindowExt:屏幕的宽高获取 扩展
9. StatusBarExt: 状态栏的高度获取 扩展
10. encryption/AES： 暂时封装了 ECB、CBC模式 扩展

#### 使用

1. ###### 添加jitpack 仓库

Android Gradle Plugin 为 v7.1.0 以下版本：进入项目根目录，打开 “build.gradle” 文件，在 “allprojects” 中加入如下代码：
```xml
...
    allprojects {
       repositories {
         maven { url 'https://jitpack.io' }
         mavenCentral()
         google()
       }
    }
```

当您的 Android Gradle Plugin 为 v7.1.0 或以上版本：进入项目根目录，打开 “settings.gradle” 文件，在
“dependencyResolutionManagement” 中加入如下代码：

```xml
...dependencyResolutionManagement{
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
       repositories {
          maven { url 'https://jitpack.io' }
          mavenCentral()
          google()
       }
    }
```

2. ###### 添加依赖

[![](https://jitpack.io/v/com.gitee.ym521/myktx.svg)](https://jitpack.io/#com.gitee.ym521/myktx)

```gradle

implementation 'com.gitee.ym521:myktx:latest version'

```


