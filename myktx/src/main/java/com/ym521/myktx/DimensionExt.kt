package com.ym521.myktx

import android.content.res.Resources
import android.util.TypedValue
import kotlin.math.roundToInt


/**
 *@author Ym
 *E-mail: 2435970206@qq.com
 *createTime:2024/4/4
 *explain:
 * 像素单位 转换
 * 屏幕相关的值
 */

/**
 * dp 转 px
 * 四舍五入
 * return Float
 */
fun dipTopx(dip: Float): Float = TypedValue.applyDimension(
    TypedValue.COMPLEX_UNIT_DIP,
    dip,
    Resources.getSystem().displayMetrics
)

/**
 * dp 转 px
 * return Int
 */
fun dipTopxI(dip: Float): Int = dipTopx(dip).roundToInt()

/**
 * sp 转 px
 * return Float
 */
fun spTopx(sp: Float): Float =
    TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, Resources.getSystem().displayMetrics)

/**
 * sp 转 px
 * 四舍五入
 * return Int
 */
fun spTopxI(sp: Float): Int = spTopx(sp).roundToInt()

/**
 * px 转 dp
 * return Float
 */
fun pxTodp(px: Float): Float = px / Resources.getSystem().displayMetrics.density

/**
 * px 转 dp
 * 四舍五入
 * return Int
 */
fun pxTodpI(px: Float): Int = pxTodp(px).roundToInt()

/**
 * px 转 sp
 * return Float
 */
fun pxTosp(px: Float): Float = px / Resources.getSystem().displayMetrics.scaledDensity

/**
 * px 转 sp
 * 四舍五入
 * return Int
 */
fun pxTospI(px: Float): Int = pxTosp(px).roundToInt()


