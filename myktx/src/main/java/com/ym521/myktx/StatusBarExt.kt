package com.ym521.myktx

import android.app.Activity
import android.graphics.Rect
import android.view.View
import android.view.Window

/**
 *@author Ym
 *E-mail: 2435970206@qq.com
 *createTime:2024/5/2
 *explain:
 * 状态栏相关
 */

/**
 * 状态栏的高度
 * return int[0] 屏幕的宽，int[1] 屏幕的高
 */
fun statusBarHeight(activity: Activity): Int {
    val rect = Rect()
    val window: Window = activity.window
    window.decorView.getWindowVisibleDisplayFrame(rect)
    val statusBarHeight: Int = rect.top
    val contentViewTop: Int = window.findViewById<View>(Window.ID_ANDROID_CONTENT).top
    return statusBarHeight - contentViewTop
}
