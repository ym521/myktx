package com.ym521.myktx

import android.content.res.Resources

/**
 *@author Ym
 *E-mail: 2435970206@qq.com
 *createTime:2024/5/2
 *explain:
 *屏幕相关
 */

/**
 * 屏幕的宽高
 * return int[0] 屏幕的宽，int[1] 屏幕的高
 */
fun screenPixelSize(): IntArray = intArrayOf(
    Resources.getSystem().displayMetrics.widthPixels,
    Resources.getSystem().displayMetrics.heightPixels
)