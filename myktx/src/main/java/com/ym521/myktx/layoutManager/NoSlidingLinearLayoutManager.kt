package com.ym521.myktx.layoutManager

import android.content.Context
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

/**
 *@author Ym
 *E-mail: 2435970206@qq.com
 *createTime:2024/4/21
 *explain:
 *
 * LinearLayoutManager的禁止滑动
 * 可以避免 不必要的滑动拉伸
 */
class NoSlidingLinearLayoutManager : LinearLayoutManager {

    constructor(context: Context) : super(context)

    constructor(
        context: Context, @RecyclerView.Orientation orientation: Int,
        reverseLayout: Boolean
    ) : super(context, orientation, reverseLayout)

    override fun canScrollHorizontally(): Boolean {
        return !(HORIZONTAL == orientation)
    }

    override fun canScrollVertically(): Boolean {
        return !(VERTICAL == orientation)
    }
}