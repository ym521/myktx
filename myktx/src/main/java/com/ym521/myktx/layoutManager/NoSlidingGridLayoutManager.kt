package com.ym521.myktx.layoutManager

import android.content.Context
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView

/**
 *@author Ym
 *E-mail: 2435970206@qq.com
 *createTime:2024/4/21
 *explain:
 *
 *GridLayoutManager的禁止滑动
 * 可以避免 不必要的滑动拉伸 提高用户体验感
 */
class NoSlidingGridLayoutManager:GridLayoutManager {

    constructor(context: Context,spanCount:Int) : super(context,spanCount)

    constructor(
        context: Context, spanCount:Int,@RecyclerView.Orientation orientation: Int,
        reverseLayout: Boolean
    ) :super(context, spanCount,orientation, reverseLayout)


    override fun canScrollHorizontally(): Boolean {
        return !(HORIZONTAL == orientation)
    }

    override fun canScrollVertically(): Boolean {
        return !(VERTICAL == orientation)
    }
}