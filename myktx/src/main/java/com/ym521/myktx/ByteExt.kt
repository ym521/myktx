package com.ym521.myktx

/**
 *@author Ym
 *E-mail: 2435970206@qq.com
 *createTime:2024/4/4
 *explain:
 *
 */

/**
 * @param unsigned 是否去除byte的符号（正负）
 */
fun Byte.toInt(unsigned: Boolean): Int {
    return if (unsigned) {
        this.toUByte().toInt()
    } else {
        this.toInt()
    }
}

/**
 *Byte 转16进制字符串
 * @param prefix 是否补0
 */
fun Byte.toHexString(prefix: Boolean = false): String {
    return if (this < 0x10) {
        "${if (prefix) "0x" else ""}0${this.toInt(true).toString(16)}"
    } else {
        "${if (prefix) "0x" else ""}${this.toInt(true).toString(16)}"
    }
}