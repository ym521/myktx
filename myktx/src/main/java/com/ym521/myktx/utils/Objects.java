package com.ym521.myktx.utils;

/**
 * @author Ym
 * E-mail: 2435970206@qq.com
 * createTime:2024/5/4
 * explain:
 */
public class Objects {

    public static <T> T requireNonNull(T obj) {
        if (obj == null)
            throw new NullPointerException();
        return obj;
    }
}
