package com.ym521.myktx.softInputKeyBoard

import android.content.Context
import android.os.Build
import android.view.View
import android.view.inputmethod.InputMethodManager

/**
 *@author Ym
 *E-mail: 2435970206@qq.com
 *createTime:2024/5/1
 *explain:
 * 软键盘的显示和隐藏
 */
object SoftInputKeyBoardHelper {

    /**
     * 显示软键盘
     */
    fun showSoftInputKeyBoard(focusView: View) {
        focusView.requestFocus()
        focusView.context.apply {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.TIRAMISU) {
                imm.showSoftInput(focusView, InputMethodManager.SHOW_FORCED)
            } else {
                imm.showSoftInput(focusView, InputMethodManager.SHOW_IMPLICIT)
            }
        }
    }

    /**
     * 隐藏软键盘
     */
    fun hideSoftInputKeyBoard(focusView: View) {
        focusView.context.apply {
            val imd = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imd.hideSoftInputFromWindow(
                focusView.windowToken,
                0
            )
        }
    }


}